﻿namespace TemplatedTabControl
{
    public delegate void TypedEventHandler<TSender, TArgs>(TSender sender, TArgs args);
    public delegate void TypedEventHandler<TArgs>(TArgs args);
}
