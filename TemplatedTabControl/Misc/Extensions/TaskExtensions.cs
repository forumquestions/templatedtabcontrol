﻿using System;
using System.Threading.Tasks;
using NLog;

namespace TemplatedTabControl.Misc.Extensions
{
    public static class TaskExtensions
    {
        public static async void LogErrors(this Task task, ILogger logger, string callerMethodName = null)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                logger.Error((callerMethodName ?? nameof(LogErrors)) + ".", ex.ToString());
            }
        }

        public static async void LogErrorsAndThen(this Task task, Action continuation, ILogger logger, string callerMethodName = null)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                logger.Error((callerMethodName ?? nameof(LogErrors)) + ".", ex.ToString());
            }
            continuation();
        }
    }
}
