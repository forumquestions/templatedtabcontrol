using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace TemplatedTabControl.ViewModel
{
    /// <summary>
    /// ������ ������������� ����������� �������.
    /// </summary>
    /// <typeparam name="T">��� ����������� ���������.</typeparam>
    public abstract class TabItemCollectionViewModelBase<T> : TabItemViewModelBase
        where T : ViewModelBase
    {
        public ObservableCollection<T> Items { get; }

        protected TabItemCollectionViewModelBase()
        {
            Items = new ObservableCollection<T>();
        }
    }
}