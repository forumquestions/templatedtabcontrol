﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace TemplatedTabControl.ViewModel
{
    /// <summary>
    /// Базовый класс для вкладки.
    /// </summary>
    public abstract class TabItemViewModelBase : ViewModelBase
    {
        private bool _isInitialized;
        private string _title;

        /// <summary> Загловок. </summary>
        public string Title
        {
            get { return _title; }
            set { Set(ref _title, value); }
        }

        /// <summary>
        /// Инициализирован ли текущий экземпляр.
        /// </summary>
        public bool IsInitialized
        {
            get { return _isInitialized; }
            set { Set(ref _isInitialized, value); }
        }

        /// <summary> Закрыть вкладку. </summary>
        public ICommand CloseCommand { get; }

        /// <summary> Запрошено закрытие вкладки. </summary>
        public event TypedEventHandler<TabItemViewModelBase> CloseRequested;

        protected TabItemViewModelBase()
        {
            CloseCommand = new RelayCommand(() => { CloseRequested?.Invoke(this); });
        }
    }
}