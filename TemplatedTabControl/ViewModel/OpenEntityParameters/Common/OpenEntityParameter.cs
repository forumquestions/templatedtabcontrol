﻿using TemplatedTabControl.Model;
using TemplatedTabControl.Prism.Interactivity.InteractionRequest;

namespace TemplatedTabControl.ViewModel.OpenEntityParameters.Common
{
    public class OpenEntityParameter : INotification
    {
        public string Title { get; set; }

        public object Content { get; set; }

        public TypeCatalog Type { get; set; }
    }
}
