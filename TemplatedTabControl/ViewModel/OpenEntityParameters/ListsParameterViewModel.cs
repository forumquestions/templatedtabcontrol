﻿using GalaSoft.MvvmLight;

namespace TemplatedTabControl.ViewModel.OpenEntityParameters
{
    public class ListsParameterViewModel : ViewModelBase
    {
        private string _number;

        public string Number
        {
            get { return _number; }
            set { Set(ref _number, value); }
        }
    }
}