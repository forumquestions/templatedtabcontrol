using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace TemplatedTabControl.ViewModel
{
    /// <summary>
    /// ������ ������������� TabControl
    /// </summary>
    public abstract class TabHostViewModelBase<TAddTabCmdParam> : ViewModelBase
    {
        private int _selectedIndex;

        /// <summary> �������. </summary>
        public ObservableCollection<TabItemViewModelBase> Tabs { get; }

        public event TypedEventHandler<TabHostViewModelBase<TAddTabCmdParam>, TabItemViewModelBase> TabRemoved;

        public ICommand AddTabCommand { get; }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { Set(ref _selectedIndex, value); }
        }

        protected TabHostViewModelBase()
        {
            Tabs = new ObservableCollection<TabItemViewModelBase>();
            Tabs.CollectionChanged += TabsOnCollectionChanged;

            AddTabCommand = new RelayCommand<TAddTabCmdParam>((parameter) =>
            {
                CreateNewTab(parameter,
                    newTabViewModel =>
                    {
                        Tabs.Add(newTabViewModel);
                        SelectedIndex = Tabs.Count - 1;
                    });
            });
        }

        /// <summary>
        /// � ����������� ������ ���������� ������ ���������� ����� �������.
        /// </summary>
        protected abstract void CreateNewTab(TAddTabCmdParam parameter, Action<TabItemViewModelBase> addNewTab);

        private void TabsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (TabItemViewModelBase item in e.NewItems)
                {
                    item.CloseRequested += TabItemOnCloseRequested;
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (TabItemViewModelBase item in e.OldItems)
                {
                    item.CloseRequested -= TabItemOnCloseRequested;
                }
            }
        }

        private void TabItemOnCloseRequested(TabItemViewModelBase item)
        {
            Tabs.Remove(item);
            TabRemoved?.Invoke(this, item);
        }
    }
}