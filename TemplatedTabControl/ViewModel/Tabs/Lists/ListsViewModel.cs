using System.Threading.Tasks;

namespace TemplatedTabControl.ViewModel.Tabs.Lists
{
    /// <summary>
    /// ������ ������������� ������ (<see cref="ListViewModel"/>).
    /// </summary>
    public class ListsViewModel : TabItemCollectionViewModelBase<ListViewModel>
    {
        public ListsViewModel()
        {
        }

        public async Task Initialize(string number)
        {
            await Task.Delay(1250); // �������� ��������� � ��.

            Title = "����� �" + number;
            IsInitialized = true;
        }
    }
}