using GalaSoft.MvvmLight;

namespace TemplatedTabControl.ViewModel.Tabs.Lists
{
    /// <summary>
    /// ������ ������������� ������ �� ������� "�����" (<see cref="ListsViewModel"/>).
    /// </summary>
    public class ListViewModel : ViewModelBase
    {
        public int Index { get; set; }

        public string Title { get; set; }
    }
}