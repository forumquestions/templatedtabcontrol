using GalaSoft.MvvmLight;

namespace TemplatedTabControl.ViewModel.Tabs.Funds
{
    /// <summary>
    /// ������ ������������� ������ �� ������� "�����" (<see cref="FundsViewModel"/>).
    /// </summary>
    public class FundViewModel : ViewModelBase
    {
        public int Number { get; set; }

        public string Name { get; set; }
    }
}