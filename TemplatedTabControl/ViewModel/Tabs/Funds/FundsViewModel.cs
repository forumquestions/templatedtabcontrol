using System.Threading.Tasks;

namespace TemplatedTabControl.ViewModel.Tabs.Funds
{
    /// <summary>
    /// ������ ������������� ������ (<see cref="FundViewModel"/>).
    /// </summary>
    public class FundsViewModel : TabItemCollectionViewModelBase<FundViewModel>
    {
        public FundsViewModel()
        {
        }

        public async Task Initialize(string number)
        {
            await Task.Delay(1250); // �������� ��������� � ��.

            Title = "����� �" + number;
            IsInitialized = true;
        }
    }
}