using System;
using NLog;
using TemplatedTabControl.Misc.Extensions;
using TemplatedTabControl.Model;
using TemplatedTabControl.Prism.Interactivity.InteractionRequest;
using TemplatedTabControl.ViewModel.OpenEntityParameters;
using TemplatedTabControl.ViewModel.OpenEntityParameters.Common;
using TemplatedTabControl.ViewModel.Tabs.Funds;
using TemplatedTabControl.ViewModel.Tabs.Lists;

namespace TemplatedTabControl.ViewModel
{
    /// <summary>
    /// ������ ������������� ����.
    /// </summary>
    public class MainViewModel : TabHostViewModelBase<TypeCatalog>
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private string _info;

        public string Info
        {
            get { return _info; }
            set { Set(ref _info, value); }
        }

        /// <summary>
        /// ������ �������������� ��� ������ ���� ���������� �����.
        /// </summary>
        public InteractionRequest<OpenEntityParameter> OpenFundsDialogRequest { get; }
        /// <summary>
        /// ������ �������������� ��� ������ ���� ���������� �����.
        /// </summary>
        public InteractionRequest<OpenEntityParameter> OpenListsDialogRequest { get; }

        public MainViewModel()
        {
            OpenFundsDialogRequest = new InteractionRequest<OpenEntityParameter>();
            OpenListsDialogRequest = new InteractionRequest<OpenEntityParameter>();

            Tabs.Add(new FundsViewModel
            {
                Title = "�����",
                IsInitialized = true,
                Items =
                {
                    new FundViewModel { Number = 1, Name = "Fund name 1" },
                    new FundViewModel { Number = 2, Name = "Fund name 2" },
                }
            });
            Tabs.Add(new ListsViewModel
            {
                Title = "�����",
                IsInitialized = true,
                Items =
                {
                    new ListViewModel { Index = 1, Title = "Inventory title 1" },
                    new ListViewModel { Index = 2, Title = "Inventory title 2" },
                }
            });
            SelectedIndex = Tabs.Count - 1;

            TabRemoved += OnTabRemoved;
        }

        /// <summary>
        /// ���������� ������ ���������� ����� �������.
        /// </summary>
        protected override void CreateNewTab(TypeCatalog type, Action<TabItemViewModelBase> addNewTab)
        {
            OpenEntityParameter parameter = null;

            switch (type)
            {
                case TypeCatalog.Funds:
                    parameter = new OpenEntityParameter
                    {
                        Title = "������� ����",
                        Type = type,
                        Content = new FundsParameterViewModel { Number = "1" }
                    };
                    break;

                case TypeCatalog.Lists:
                    parameter = new OpenEntityParameter
                    {
                        Title = "������� �����",
                        Type = type,
                        Content = new ListsParameterViewModel { Number = "1" }
                    };
                    break;

                case TypeCatalog.Cases:

                    break;

                case TypeCatalog.Documents:

                    break;

                case TypeCatalog.Resolution:

                    break;

                case TypeCatalog.Enterprises:

                    break;


                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }

            _logger.Debug($"{nameof(CreateNewTab)}. TypeCatalog = {type.ToString()}");

            switch (type)
            {
                case TypeCatalog.Funds:
                    OpenFundsDialogRequest.Raise(parameter, confirmation =>
                    {
                        var dialogResult = (FundsParameterViewModel)confirmation.Content;
                        var item = new FundsViewModel { Title = "��������..." };

                        // ��������� ������ ���������� � ���������� ������ � ������������.
                        item.Initialize(dialogResult.Number)
                            .LogErrors(_logger, nameof(CreateNewTab));

                        _logger.Debug($"{nameof(CreateNewTab)}. TypeCatalog = {type.ToString()}. ����� ������� ���������.");
                        addNewTab(item);
                    });
                    break;

                case TypeCatalog.Lists:
                    OpenListsDialogRequest.Raise(parameter, confirmation =>
                    {
                        var dialogResult = (ListsParameterViewModel)confirmation.Content;
                        var item = new ListsViewModel { Title = "��������..." };

                        // ��������� ������ ���������� � ���������� ������ � ������������.
                        item.Initialize(dialogResult.Number)
                            .LogErrors(_logger, nameof(CreateNewTab));

                        _logger.Debug($"{nameof(CreateNewTab)}. TypeCatalog = {type.ToString()}. ����� ������� ���������.");
                        addNewTab(item);
                    });

                    break;

                case TypeCatalog.Cases:

                    break;

                case TypeCatalog.Documents:

                    break;

                case TypeCatalog.Resolution:

                    break;

                case TypeCatalog.Enterprises:

                    break;


                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }

        private void OnTabRemoved(TabHostViewModelBase<TypeCatalog> sender, TabItemViewModelBase item)
        {
            string message = $"������� \"{item.Title}\" ���� �������.";
            _logger.Debug($"{nameof(OnTabRemoved)}. {message}");

            Info = message;
        }
    }
}