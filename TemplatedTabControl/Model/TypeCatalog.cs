﻿namespace TemplatedTabControl.Model
{
    public enum TypeCatalog
    {
        None,
        Funds,
        Lists,
        Cases,
        Documents,
        Resolution,
        Enterprises
    }
}