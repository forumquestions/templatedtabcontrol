﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Markup;
using TemplatedTabControl.Prism.Interactivity.InteractionRequest;
using TemplatedTabControl.ViewModel.OpenEntityParameters.Common;
using TemplatedTabControl.Views;

namespace TemplatedTabControl.EventTriggers.Dialogs
{
    namespace OpeningTheWindowsExample.TriggerActions
    {
        /// <summary>
        /// Действие по показу дочернего окна.
        /// </summary>
        [ContentProperty("ContentDataTemplate")]
        public class OpenEntityDialogAction : TriggerAction<UIElement>
        {
            /// <summary>
            /// Шаблон, для отображения контента.
            /// </summary>
            public DataTemplate ContentDataTemplate { get; set; }

            protected override void Invoke(object parameter)
            {
                var args = (InteractionRequestedEventArgs)parameter;
                var context = (OpenEntityParameter)args.Context;

                // Получаем ссылку на окно, содержащее объект, в который помещён триггер.
                Window parentWindows = Window.GetWindow(AssociatedObject);

                // Обрабатываем делегат обратного вызова при закрытии окна.
                void OnChildWindowClosed(object sender, EventArgs e)
                {
                    var window = (Window)sender;
                    window.Closed -= OnChildWindowClosed;
                    window.Loaded -= OnChildWindowLoaded;

                    if (args.Callback != null)
                    {
                        parentWindows.Tag = null;
                        args.Callback();
                    }
                }

                void OnChildWindowLoaded(object sender, RoutedEventArgs e)
                {
                    var window = (Window)sender;
                    window.Content = context.Content;
                }

                if (parentWindows.Tag == null)
                {
                    // Создаём дочернее окно, устанавливаем его содержимое и его шаблон.
                    var childWindows = new OpenEntityDialog
                    {
                        Title = context.Title,
                        Owner = parentWindows,
                        WindowStyle = WindowStyle.SingleBorderWindow,
                        SizeToContent = SizeToContent.WidthAndHeight,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        ContentTemplate = ContentDataTemplate,
                    };

                    childWindows.Loaded += OnChildWindowLoaded;
                    childWindows.Closed += OnChildWindowClosed;

                    parentWindows.Tag = childWindows;

                    // Показываем диалог.
                    childWindows.Show();
                }
                else
                {
                    var window = (Window)parentWindows.Tag;
                    window.Close();
                }
            }
        }
    }
}
