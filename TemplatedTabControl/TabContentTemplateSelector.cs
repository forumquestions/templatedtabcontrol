﻿using System.Windows;
using System.Windows.Controls;
using TemplatedTabControl.ViewModel;
using TemplatedTabControl.ViewModel.Tabs.Funds;
using TemplatedTabControl.ViewModel.Tabs.Lists;

namespace TemplatedTabControl
{
    public class TabContentTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FundsDataTemplate { get; set; }

        public DataTemplate ListDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is FundsViewModel)
            {
                return FundsDataTemplate;
            }
            if (item is ListsViewModel)
            {
                return ListDataTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}
